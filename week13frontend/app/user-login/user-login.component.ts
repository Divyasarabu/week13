import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServiceService } from '../service/user-service.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-user-login',
  templateUrl: './user-login.component.html',
  styleUrls: ['./user-login.component.css']
})
export class UserLoginComponent implements OnInit {

  myRouter:Router;

  user=new FormGroup({
    name:new FormControl(''),
    password:new FormControl('')
  })

  myUserServiceService: UserServiceService;

  myActivatedRoute:ActivatedRoute;

  constructor(activatedRoute:ActivatedRoute, router:Router, userServiceService: UserServiceService){
    this.myRouter=router;
    this.myActivatedRoute=activatedRoute;
    this.myUserServiceService=userServiceService;
   }

  ngOnInit(): void {
  }
  
  loginUser(){
    let loginUser = {  
      name:this.user.value.name,
      password:this.user.value.password
    }
    
    this.myUserServiceService.loginUser(loginUser).subscribe(result => this.gotoUserPage());
    }

    gotoUserPage() {
      this.myRouter.navigate(['/user-page',this.user.value.name]);
    }

    signup() {
      this.myRouter.navigate(['/user-registration']);
    }
}
