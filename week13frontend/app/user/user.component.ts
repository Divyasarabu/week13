import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServiceService } from '../service/user-service.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  id:any;
  name:any;
  email:any;

  myActivatedRoute:ActivatedRoute;

  myRouter:Router;

  myUserServiceService: UserServiceService;

  userList:any = [];

  constructor(activatedRoute:ActivatedRoute, router:Router, userServiceService: UserServiceService) {
    this.myActivatedRoute=activatedRoute;
    this.myRouter=router;
    this.myUserServiceService=userServiceService;
   }

   listUser(){
    this.myUserServiceService.list().subscribe((response)=>{
      this.userList = response;
    },(error=>{

    }));
  }

  ngOnInit(): void {
    this.name=this.myActivatedRoute.snapshot.paramMap.get('name');
    this.listUser();
  }

  deleteUser(id:number,name:String,email:String){
    let deleteUser={
      id:id,
      name:name,
      email:email
    }
    
    this.myUserServiceService.delete(deleteUser).subscribe(result => this.gotoAdminPage());
      window.alert("User is deleted...");
    }

  gotoAdminPage() {
    this.myRouter.navigate(['/admin',this.name]);
  }
}
