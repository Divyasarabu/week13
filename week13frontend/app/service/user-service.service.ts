import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  showUserUrl: string = 'http://localhost:9091/showUsers';
  saveUserUrl: string = 'http://localhost:9091/saveUser';
  updateUrl: string = 'http://localhost:9091/updateUser';
  deleteUrl: string = 'http://localhost:9091/deleteUser';
  loginUrl: string = 'http://localhost:9091/loginUser';

  constructor(private http: HttpClient) { }

  // Create
  create(data: any): Observable<any> {
    let API_URL = `${this.saveUserUrl}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.handleError)
      )
  }

  loginUser(data: any): Observable<any> {
    let API_URL = `${this.loginUrl}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.handleError)
      )
  }

  // Read
  list() {
    return this.http.get(`${this.showUserUrl}`);
  }

  // Update
  update(data: any): Observable<any> {
    let API_URL = `${this.updateUrl}`;
    return this.http.put(API_URL, data).pipe(
      catchError(this.handleError)
    )
  }

  // Delete
  delete(data: any): Observable<any> {
    var API_URL = `${this.deleteUrl}`;
    return this.http.post(API_URL,data).pipe(
      catchError(this.handleError)
    )
  }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  };

}
