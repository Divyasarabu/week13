import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BookServiceService {

  showBooksUrl: string = 'http://localhost:9091/books';
  addBookUrl: string = 'http://localhost:9091/addBook';
  updateBookUrl: string = 'http://localhost:9091/updateBook';
  deleteBookUrl: string = 'http://localhost:9091/deleteBook';


  constructor(private http: HttpClient) { }

  // Create
  create(data: any): Observable<any> {
    let API_URL = `${this.addBookUrl}`;
    return this.http.post(API_URL, data)
      .pipe(
        catchError(this.handleError)
      )
  }

  // Read
  list() {
    return this.http.get(`${this.showBooksUrl}`);
  }

    // Update
    update(data: any): Observable<any> {
      let API_URL = `${this.updateBookUrl}`;
      return this.http.put(API_URL, data).pipe(
        catchError(this.handleError)
      )
    }
  
    // Delete
    delete(data: any): Observable<any> {
      var API_URL = `${this.deleteBookUrl}`;
      return this.http.post(API_URL,data).pipe(
        catchError(this.handleError)
      )
    }

  // Handle API errors
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  };

}




