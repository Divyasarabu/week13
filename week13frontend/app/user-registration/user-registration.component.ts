import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserServiceService } from '../service/user-service.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})

export class UserRegistrationComponent implements OnInit {
  
  user=new FormGroup({
    name:new FormControl(''),
    email:new FormControl(''),
    password:new FormControl('')
  })

  myUserServiceService: UserServiceService;

  myActivatedRoute:ActivatedRoute;

  myRouter:Router;

  constructor(activatedRoute:ActivatedRoute, router:Router, userServiceService: UserServiceService){
    this.myRouter=router;
    this.myActivatedRoute=activatedRoute;
    this.myUserServiceService=userServiceService;
   }

  ngOnInit(): void {
  }
  
  addUser(){
    let addUser = {  
      name:this.user.value.bookname,
      email:this.user.value.email,
      password:this.user.value.password
    }
    
    this.myUserServiceService.create(addUser).subscribe(result => this.gotoUserLogin())
      window.alert("your are register successful ");
    }
    gotoUserLogin() {
      this.myRouter.navigate(['/user-login']);
    }
}

