import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BookServiceService } from 'src/app/service/book-service.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  myRouter:Router;

  myBookServiceService: BookServiceService;

  bookList:any = [];

  constructor(router:Router, bookServiceService: BookServiceService) {

    this.myRouter=router;
    this.myBookServiceService=bookServiceService;
   }

   listBooks(){
    this.myBookServiceService.list().subscribe((response)=>{
      this.bookList = response;
    },(error=>{

    }));
  }

  ngOnInit(): void {
    this.listBooks();
  }

}
