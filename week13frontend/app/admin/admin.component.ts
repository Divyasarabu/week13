import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router } from '@angular/router';
import { BookServiceService } from 'src/app/service/book-service.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  
  book_id:any;
  title:any;
  total_pages:any;
  rating:any;
  isbn:any;

  myActivatedRoute:ActivatedRoute;

  myRouter:Router;

  myBookServiceService: BookServiceService;

  name:any;

  bookList:any = [];

  constructor(activatedRoute:ActivatedRoute, router:Router, bookServiceService: BookServiceService) {
    this.myActivatedRoute=activatedRoute;
    this.myRouter=router;
    this.myBookServiceService=bookServiceService;
   }

   listBooks(){
    this.myBookServiceService.list().subscribe((response)=>{
      this.bookList = response;
    },(error=>{

    }));
  }

  ngOnInit(): void {
    this.name=this.myActivatedRoute.snapshot.paramMap.get('name');
    this.listBooks();
  }

  deleteBook(book_id:number,title:String,total_pages:number,rating:number,isbn:number){
    let deleteBook={
      book_id:book_id,
      title:title,
      total_pages:total_pages,
      rating:rating,
      isbn:isbn
    }
    
    this.myBookServiceService.delete(deleteBook).subscribe((response)=>{
      window.alert("your Book is deleted...");
      this.listBooks();
    },(error=>{
    }));
  }

  updateBook(book_id:number,title:String,total_pages:number,rating:number,isbn:number,name:String){
      
        this.myRouter.navigate(['update-book',book_id,title,total_pages,rating,isbn,name]);
    
      }
  addBook(){
      
        this.myRouter.navigate(['add-book', this.name]);
    
    }       
  viewUsers(){
      
        this.myRouter.navigate(['users', this.name]);
    
      }    
}