import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-admin-login',
  templateUrl: './admin-login.component.html',
  styleUrls: ['./admin-login.component.css']
})

export class AdminLoginComponent implements OnInit {
  
  myRouter:Router;

  admin=new FormGroup({
    name:new FormControl(''),
    password:new FormControl('')
  })

  myAdminServiceService: AdminServiceService;

  myActivatedRoute:ActivatedRoute;

  constructor(activatedRoute:ActivatedRoute, router:Router, adminServiceService: AdminServiceService){
    this.myRouter=router;
    this.myActivatedRoute=activatedRoute;
    this.myAdminServiceService=adminServiceService;
   }

  ngOnInit(): void {
  }
  
  loginAdmin(){
    let loginAdmin = {  
      name:this.admin.value.name,
      password:this.admin.value.password
    }
    
    this.myAdminServiceService.loginAdmin(loginAdmin).subscribe(result => this.gotoAdminPage());
    }

    gotoAdminPage() {
      this.myRouter.navigate(['/admin',this.admin.value.name]);
    }

    signup() {
      this.myRouter.navigate(['/admin-registration']);
    }
}
