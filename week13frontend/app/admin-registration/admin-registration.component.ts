import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AdminServiceService } from '../service/admin-service.service';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-admin-registration',
  templateUrl: './admin-registration.component.html',
  styleUrls: ['./admin-registration.component.css']
})

export class AdminRegistrationComponent implements OnInit {
  
  admin=new FormGroup({
    name:new FormControl(''),
    email:new FormControl(''),
    password:new FormControl(''),
    systemCode:new FormControl('')
  })

  myAdminServiceService: AdminServiceService;

  myActivatedRoute:ActivatedRoute;

  constructor(activatedRoute:ActivatedRoute, adminServiceService: AdminServiceService){
    this.myActivatedRoute=activatedRoute;
    this.myAdminServiceService=adminServiceService;
   }

  ngOnInit(): void {
  }
  
  addAdmin(){
    let addAdmin = {  
      name:this.admin.value.bookname,
      email:this.admin.value.email,
      password:this.admin.value.password,
      systemCode:this.admin.value.systemCode
    }
    
    this.myAdminServiceService.create(addAdmin).subscribe((response)=>{
      window.alert("your are register successful ");
    },(error=>{
      window.alert("AdminId is dublicated");
    }));
  }
}
